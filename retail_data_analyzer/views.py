from django.shortcuts import render
from django_tables2 import RequestConfig
from .tables import KeyIndicatorsTable, ProdSellsTable
import pandas as pd
import numpy as np
import datetime
from dwapi import datawiz

dw = datawiz.DW()
_date_from = datetime.date(2015, 11, 17)
_date_to = datetime.date(2015, 11, 18)
_shops = list(dw.get_shops().keys())
pd.set_option('display.expand_frame_repr', False)  # for displaying df in console in full mode


def key_indicators(request):
    # From local csv:
    # df_turn = pd.read_csv("local/df_turn", sep='\t')
    # df_goods_qty = pd.read_csv("local/df_goods_qty", sep='\t')
    # df_recipes_qty = pd.read_csv("local/df_recipes_qty", sep='\t')

    # From API:
    df_turn = dw.get_categories_sale(categories=["sum"],
                                     by='turnover',
                                     # show='name',
                                     # view_type='represent',
                                     shops=_shops,
                                     date_from=_date_from,
                                     date_to=_date_to,
                                     interval=datawiz.DAYS)
    df_goods_qty = dw.get_categories_sale(categories=["sum"],
                                          by='qty',
                                          # show='name',
                                          # view_type='represent',
                                          shops=_shops,
                                          date_from=_date_from,
                                          date_to=_date_to,
                                          interval=datawiz.DAYS)
    df_recipes_qty = dw.get_categories_sale(categories=["sum"],
                                            by='receipts_qty',
                                            # show='name',
                                            # view_type='represent',
                                            shops=_shops,
                                            date_from=_date_from,
                                            date_to=_date_to,
                                            interval=datawiz.DAYS)
    df = _create_key_values_df(df_turn, df_goods_qty, df_recipes_qty)

    table = KeyIndicatorsTable(df.T.to_dict().values())
    RequestConfig(request).configure(table)
    return render(request, 'basic_bootstrap_table.html', {'table': table, 'title': 'Key indicators'})


def _create_key_values_df(df_turn, df_qty, df_rec):
    df_turn = df_turn.rename(columns={0: 'turnover'}).T
    df_qty = df_qty.rename(columns={0: 'qty'}).T
    df_rec = df_rec.rename(columns={0: 'receipt_qty'}).T
    df = pd.concat([df_turn, df_qty, df_rec]).reset_index()
    df = df.reindex(columns=['2015-11-18', '2015-11-17'])  # set 18' date to be first column
    df = df.rename(columns={'2015-11-18': 'date_to', '2015-11-17': 'date_from'})
    # df = df.drop(df.index[[2, 4]])
    df.loc[3] = np.nan
    df.insert(0, 'indicator', ['Оборот', 'Кількість товарів', 'Кількість чеків', 'Середній чек'])
    df.insert(3, 'diff_by_percents', np.nan)
    df.insert(4, 'diff_by_values', np.nan)
    # df['date_from'] = df['date_from'].shift(-1)
    # df['date_to'] = df['date_to'].shift(-1)

    # calculate medium check value
    df.iat[3, 1] = round(df.iat[0, 1] / df.iat[2, 1], 2)
    df.iat[3, 2] = round(df.iat[0, 2] / df.iat[2, 2], 2)

    # calculate diff by %
    df['diff_by_percents'] = df.apply(
        lambda x: "{0:.2f}".format((x['date_to'] - x['date_from']) / x['date_from'] * 100),
        axis=1)

    # calculate diff by value
    df['diff_by_values'] = df.apply(
        lambda x: format(round(x['date_to'] - x['date_from'], 2), '.15g'),  # .15g to drop trailing .0 in floats
        axis=1)

    return df


def prod_sells(request, slug='inc'):
    # From local csv:
    # df_prod_turn = pd.read_csv("local/df_prod_turn", sep='\t')
    # df_qty_of_sells = pd.read_csv("local/df_qty_of_sells", sep='\t')

    # From API:
    df_prod_turn = dw.get_products_sale(categories=["sum"],
                                        by='turnover',
                                        show='name',
                                        view_type='represent',
                                        shops=_shops,
                                        date_from=_date_from,
                                        date_to=_date_to,
                                        interval=datawiz.DAYS).reset_index()
    df_qty_of_sells = dw.get_products_sale(categories=["sum"],
                                           by='qty',
                                           show='name',
                                           view_type='represent',
                                           shops=_shops,
                                           date_from=_date_from,
                                           date_to=_date_to,
                                           interval=datawiz.DAYS).reset_index()
    df = _create_prod_sells_rate_df(df_prod_turn, df_qty_of_sells, direction=slug)

    table = ProdSellsTable(df.T.to_dict().values())
    RequestConfig(request).configure(table)
    return render(request, 'basic_bootstrap_table.html',
                  {'table': table, 'title': 'Products with ' + slug + 'reased sells indicators'})


def _create_prod_sells_rate_df(df_prod_turn, df_qty_of_sells, direction):
    df_turn = df_prod_turn.rename(columns={0: 'turnover'}).T.reset_index()
    df_qty = df_qty_of_sells.rename(columns={0: 'qty'}).T.reset_index()
    df = pd.merge(df_turn, df_qty, on='name')
    df.insert(5, 'diff_by_turnover', np.nan)
    df.insert(6, 'diff_by_qty', np.nan)
    df = df.rename(columns={'name': 'prod_name', '0_x': 'turn_date_from', '1_x': 'turn_date_to',
                            '0_y': 'qty_date_from', '1_y': 'qty_date_to'})
    df = df.drop(df.index[[0]])  # remove date row

    # calculate diff by turnover of sold products
    df['diff_by_turnover'] = df.apply(
        # lambda x: "{0:.2f}".format(x['turn_date_to'] - x['turn_date_from']), axis=1)
        lambda x: x['turn_date_to'] - x['turn_date_from'], axis=1)

    # calculate diff by qty of sold products
    df['diff_by_qty'] = df.apply(
        lambda x: x['qty_date_to'] - x['qty_date_from'], axis=1)

    df = df[['prod_name', 'diff_by_qty', 'diff_by_turnover']]
    if direction == 'dec':
        df = df.query("diff_by_qty < 0 and diff_by_turnover < 0")
        df['diff_by_turnover'] = df['diff_by_turnover'].apply(lambda x: "{0:.2f}".format(x))
        df['diff_by_qty'] = df['diff_by_qty'].apply(lambda x: format(x, '.15g'))
        return df
    elif direction == 'inc':
        df = df.query("diff_by_qty > 0 and diff_by_turnover > 0")
        df['diff_by_turnover'] = df['diff_by_turnover'].apply(lambda x: "{0:.2f}".format(x))
        df['diff_by_qty'] = df['diff_by_qty'].apply(lambda x: format(x, '.15g'))
        return df
    else:
        return df
